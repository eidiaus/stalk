#pragma once
enum Parent {
	PARENT_PLAYER           = 0,
	PARENT_NPC              = 1,
	PARENT_BOSS             = 2
};

enum EnemyType
{
	ENEMY_MELEE             = 1,
	ENEMY_CASTER            = 2,
	ENEMY_BOSS              = 4
};

enum PowerupType
{
	POWERUP_HEALTH_POTION   = 0,
	POWERUP_BULLET_ENHANCER = 1
};

enum UIType
{
	UI_EMPTY_HEART          = 0,
	UI_HALF_HEART           = 1,
	UI_FULL_HEART           = 2
};