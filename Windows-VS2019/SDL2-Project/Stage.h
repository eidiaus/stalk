#ifndef STAGE_H_
#define STAGE_H_

class Stage
{
private:
	int stageid; //id populated by finding position in vector
	int enemycount; //count of enemies
	int enemytype; //EnemyType Enums type identifier
public:
	Stage(); //Constructor
	~Stage(); //Destructor

	//Initialise method
	void init(int stageid, int enemycount, int enemytype);
	int getEnemyCount();
	int getEnemyType();
	int getStageID();

};

#endif