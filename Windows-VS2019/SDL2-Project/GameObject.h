#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

#include "SDL2Common.h"
#include "Vector2f.h"
#include "AABB.h"
#include <string>
using std::string;

// Forward declarations
// improve compile time. 
class Vector2f;
class AABB;

class GameObject
{
protected:

    int SPRITE_HEIGHT = NULL;
    int SPRITE_WIDTH = NULL;
    // Texture which stores the sprite sheet (this
    // will be optimised).
    SDL_Texture* texture;
    SDL_Rect targetRectangle;
    Vector2f* position;
    float scale = 1;
    string tag;
    bool visible = true;
    // bounding box
    AABB* aabb;

public:
    GameObject();

    virtual ~GameObject();

    virtual void init(SDL_Renderer* renderer, string texturePath, Vector2f* position);

    virtual void init(SDL_Renderer* renderer, SDL_Texture* texture, Vector2f* position);

    virtual void draw(SDL_Renderer* renderer);

    Vector2f* getPosition();
    void setPosition(Vector2f* pos);

    bool isVisible();

    void setTag(string tag);

    string getTag();

    void hide();
    void show();
    // Bounding box
    AABB* getAABB();
};

#endif