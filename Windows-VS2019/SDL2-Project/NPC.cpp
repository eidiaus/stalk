#include "NPC.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Player.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

/**
 * NPC
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
NPC::NPC() : Sprite()
{
    bossVelocity = nullptr;
    cooldownTimer = 0;
    game = nullptr;
    type = 0;
    setTag("NPC");
    COOLDOWN_MAX = 0.8f;
    state = IDLE;
    speed = 30.0f;
    scale = 1;
    maxRange = 0.4f;
    timeToTarget = 0.25f;

    SPRITE_WIDTH = 32;
    SPRITE_HEIGHT = 32;

    // Could pass these in to change the difficulty
    // of a NPC for each level etc. 
    maxRange = 100.0f;
    timeToTarget = 0.25f;
    MAX_HEALTH = 100;
    health = MAX_HEALTH;
    points = 10;
}

/**
 * init
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void NPC::init(SDL_Renderer *renderer, Vector2f* initPos, int type)
{
    this->type = type;
    //path string
    string path = "";

    //postion
    position = new Vector2f(initPos);

    switch (this->type)
    {
    case ENEMY_CASTER:
        path = "assets/images/minimare-001.png";
        SPRITE_WIDTH = 32;
        SPRITE_HEIGHT = 32;
        // Call sprite constructor
        Sprite::init(renderer, path, 6, position);


        // Setup the animation structure
        animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3, 0);
        animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1, 0);
        animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0, 0);
        animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2, 0);
        animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3, 0);
        animations[DEAD]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3, 0);

        for (int i = 0; i < maxAnimations; i++)
        {
            animations[i]->setMaxFrameTime(0.4f);
        }

        animations[DEAD]->setLoop(false);
        break;
    case ENEMY_MELEE:
        path = "assets/images/undeadking-updated.png";
        SPRITE_WIDTH = 32;
        SPRITE_HEIGHT = 64;
        // Call sprite constructor
        Sprite::init(renderer, path, 6, position);


        // Setup the animation structure
        animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1, 0);
        animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2, 0);
        animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3, 0);
        animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0, 0);
        animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0, 0);
        animations[DEAD]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 4, 0);


        for (int i = 0; i < maxAnimations; i++)
        {
            animations[i]->setMaxFrameTime(0.4f);
        }

        animations[DEAD]->setLoop(false);
        break;


    case ENEMY_BOSS:
        path = "assets/images/boss_spritesheet.png";
        MAX_HEALTH = 1000;
        health = 1000;
        scale = 5;
        speed = 100.0f;
        state = RIGHT;
        COOLDOWN_MAX = 0.6f;
        position = new Vector2f(300, -50);
        SPRITE_WIDTH = 42;
        SPRITE_HEIGHT = 42;
        // Call sprite constructor
        Sprite::init(renderer, path, 2, position);


        // Setup the animation structure
        animations[LEFT]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1, 0);
        animations[RIGHT]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1, 0);

        for (int i = 0; i < maxAnimations; i++)
        {
            animations[i]->setMaxFrameTime(0.1f);
        }
        break;
    }

    aabb = new AABB(this->getPosition(), targetRectangle.h, targetRectangle.w);
}

/**
 * ~NPC
 * 
 * Destroys the NPC and any associated 
 * objects 
 * 
 */
NPC::~NPC()
{

}

void NPC::update(float dt)
{
    if(state != DEAD) //don't move dead sprite
        ai();
    else    
    {
        velocity->setX(0.0f);
        velocity->setY(0.0f);
    }
    
    Sprite::update(dt);
    cooldownTimer += dt;
}

/**
 * ai
 * 
 * Adjusts the velocity / state of the NPC
 * to follow the player
 * 
 */
void NPC::ai()
{
    // get player
    Player* player = game->getPlayer();

    // get distance to player
        // copy the player position
    Vector2f vectorToPlayer(player->getPosition());
    // subtract our position to get a vector to the player
    vectorToPlayer.sub(position);
    float distance = vectorToPlayer.length();

    if (type == ENEMY_MELEE)
    {


        // else - head for player

        // Could do with an assign in vector
        velocity->setX(vectorToPlayer.getX());
        velocity->setY(vectorToPlayer.getY());

        // will work but 'wobbles'
        //velocity->scale(speed);   

        // Arrive pattern 
        velocity->scale(timeToTarget);

        if (velocity->length() > speed)
        {
            velocity->normalise();
            velocity->scale(speed);
        }

        state = IDLE;

        if (velocity->getY() > 0.1f)
            state = DOWN;
        else
            if (velocity->getY() < -0.1f)
                state = UP;
        // empty else is not an error!

        if (velocity->getX() > 0.1f)
            state = RIGHT;
        else
            if (velocity->getX() < -0.1f)
                state = LEFT;
        // empty else is not an error.
    }
    else if (type == ENEMY_CASTER)
    {
        // if player 'in range' stop and fire
        if (distance < maxRange)
        {
            idleVelocity.setX(vectorToPlayer.getX());
            idleVelocity.setY(vectorToPlayer.getY());
            if (velocity->length() > 0.0f)
            {
                idleVelocity = velocity;
                idleVelocity.normalise();
                idleVelocity.scale(speed);
            }

            fire();
            velocity->zero();
        }
        else
        {
            velocity->setX(vectorToPlayer.getX());
            velocity->setY(vectorToPlayer.getY());
            velocity->scale(timeToTarget);
            if (velocity->length() > speed)
            {
                velocity->normalise();
                velocity->scale(speed);
            }
        }

        state = IDLE;
        if (velocity->length() > 0.0f)
        {
            if (velocity->getY() > 0.1f)
                state = DOWN;
            else
                if (velocity->getY() < -0.1f)
                    state = UP;
            // empty else is not an error!

            if (velocity->getX() > 0.1f)
                state = RIGHT;
            else
                if (velocity->getX() < -0.1f)
                    state = LEFT;
        }
        else
        {
            if (idleVelocity.getY() > 0.1f)
                state = DOWN;
            else
                if (idleVelocity.getY() < -0.1f)
                    state = UP;
            // empty else is not an error!

            if (idleVelocity.getX() > 0.1f)
                state = RIGHT;
            else
                if (idleVelocity.getX() < -0.1f)
                    state = LEFT;
        }
    }
    else if (type == ENEMY_BOSS)
    {
        idleVelocity.setX(vectorToPlayer.getX() - 23 * scale);
        idleVelocity.setY(vectorToPlayer.getY() - 23 * scale);
        
        fire();

        if (state == RIGHT)
        {
            bossVelocity = new Vector2f(1.0f, 0);
            if (position->getX() >= (game->WINDOW_WIDTH - (SPRITE_WIDTH*scale)))
            {
                bossVelocity = new Vector2f(-1.0f, 0);
                state = LEFT;
            }
        }
        if (state == LEFT)
        {
            if (position->getX() <= 0)
            {
                bossVelocity = new Vector2f(1.0f, 0);
                state = RIGHT;
            }
        }
        bossVelocity->normalise();
        bossVelocity->scale(speed);

        velocity = bossVelocity;
    }

    //state = LEFT;
    // empty else is not an error.
}

void NPC::fire()
{
    // Need a cooldown timer, otherwise we shoot 
    // a million bullets a second and our npc
    // dies instantly
    Player* player = game->getPlayer();
    Vector2f vectorToPlayer(player->getPosition());
    // subtract our position to get a vector to the player
    vectorToPlayer.sub(position);
    if (cooldownTimer > COOLDOWN_MAX)
    {
        if (type == ENEMY_CASTER)
            game->createBullet(position, &idleVelocity, PARENT_NPC);
        if (type == ENEMY_BOSS)
        {
            Vector2f* fixedPosition = new Vector2f(position->getX() + 23 * scale, position->getY() + 23 * scale);
            game->createBullet(fixedPosition, &idleVelocity, PARENT_BOSS);

        }
        cooldownTimer = 0;
    }
}

void NPC::setGame(Game* game)
{
    this->game = game;
}

int NPC::getCurrentAnimationState()
{
    if (!(this->type == ENEMY_BOSS && this->state == DEAD))
        return state;
    else
        return LEFT;
}

void NPC::takeDamage(int damage)
{
    health -= damage;

    if (health <= 0)
    {
        game->getPlayer()->addKill();
        state = DEAD;
        //game->createPowerup(position, POWERUP_HEALTH_POTION);
        int gen = rand() % 10;
        if (gen == 5 || gen == 6 || gen == 7)
        {
            if (rand() % 2 == 0)
                game->createPowerup(position, POWERUP_HEALTH_POTION);
            else
                game->createPowerup(position, POWERUP_BULLET_ENHANCER);
        }
    }
}

bool NPC::isDead()
{
    if (health > 0)
        return false;
    else 
        return true;
}

void NPC::respawn(const int MAX_HEIGHT, const int MAX_WIDTH)
{
    health = 100;
    state = IDLE;

    Vector2f randomPostion;

    int doubleWidth = MAX_WIDTH * 2;
    int doubleHeight = MAX_HEIGHT *2;

    // get a random number between 0 and 
    // 2 x screen size. 
    int xCoord = rand()%doubleWidth;
    int yCoord = rand()%doubleHeight;

    // if its on screen move it off. 
    if(xCoord < MAX_WIDTH)
        xCoord *= -1;

    if(yCoord < MAX_HEIGHT)
        yCoord *= -1; 

    randomPostion.setX(xCoord);
    randomPostion.setY(yCoord);

    this->setPosition(&randomPostion);    
}
    
int NPC::getPoints()
{
    return points;
}

void NPC::setPoints(int pointValue)
{
    points = pointValue;
}

int NPC::getHealth()
{
    return health;
}
    
int NPC::getType()
{
    return type;
}

int NPC::getMaxHealth()
{
    return MAX_HEALTH;
}