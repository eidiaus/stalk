#include "Health.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

Health::Health()
{
    //scale = 20;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;
    targetRectangle.x = 0;
    targetRectangle.y = 0;
}

void Health::init(SDL_Renderer* renderer)
{
    targetRectangle.x = 600;
    targetRectangle.y = 200;
    string path("assets/images/heart_shaded.png");

    //postion
    Vector2f position(600.0f, 200.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 1, &position);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), targetRectangle.h, targetRectangle.w);

}

Health::~Health()
{

}


void Health::setGame(Game* game)
{
    this->game = game;
}

void Health::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);

}