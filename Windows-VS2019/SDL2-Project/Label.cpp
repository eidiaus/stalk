/*Original system made by Eesaa...
v1.0*/

#include "Label.h"
#include "Vector2f.h"

/**
 * Label
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */
Label::Label()
{
    setTag("Label");
    targetRectangle.w = 0;
    targetRectangle.h = 0;
    targetRectangle.x = 0;
    targetRectangle.y = 0;
    surface = nullptr;
    texture = nullptr;
    text = nullptr;
    font = nullptr;
    color = { 255, 255, 255 };

}

Label::~Label()
{
    TTF_CloseFont(font);

}
void Label::Initialise(SDL_Renderer* renderer, Vector2f* initPos, string text, TTF_Font* font, SDL_Color color) //main init class
{
    this->position = initPos; //init pos

    this->text = text.c_str(); //init text attributes 
    this->font = font;
    this->color = color;

    if (surface != nullptr)
        SDL_FreeSurface(surface); //free surface before creating a new one
    surface = TTF_RenderText_Blended(this->font, this->text, this->color); //render text with blended mode for smoother edges
    if (texture != nullptr)
        SDL_DestroyTexture(texture); //destroy texture before generating new one
    texture = SDL_CreateTextureFromSurface(renderer, surface); //create texture from the surface we created previously

    targetRectangle.x = position->getX(); //gameobject has no position setting implemented, will do in future iteration of engine.
    targetRectangle.y = position->getY();

    GameObject::init(renderer, texture, position); //init gameobject
}

void Label::init(SDL_Renderer* renderer, Vector2f* initPos, string text) //pass through only text string and use default font (sans)
{
    string fontPath = "assets/fonts/Sans.ttf";
    font = TTF_OpenFont(fontPath.c_str(), 24);
    Initialise(renderer, initPos, text, font, color);
}

void Label::init(SDL_Renderer* renderer, Vector2f* initPos, string text, SDL_Color color) //pass through text string and color but use default font
{
    string fontPath = "assets/fonts/Sans.ttf";
    font = TTF_OpenFont(fontPath.c_str(), 24);
    Initialise(renderer, initPos, text, font, color);
}

void Label::init(SDL_Renderer* renderer, Vector2f* initPos, string text, TTF_Font* font) //pass through text string and font but use default color
{
    Initialise(renderer, initPos, text, font, color);
}


void Label::init(SDL_Renderer* renderer, Vector2f* initPos, string text, TTF_Font* font, SDL_Color color) //pass through text string, font and color
{
    Initialise(renderer, initPos, text, font, color);
}


void Label::update(SDL_Renderer* renderer)
{
    if (surface != nullptr)
        SDL_FreeSurface(surface);
    surface = TTF_RenderText_Blended(this->font, this->text, this->color);
    if (texture != nullptr)
        SDL_DestroyTexture(texture);
    texture = SDL_CreateTextureFromSurface(renderer, surface);

    GameObject::init(renderer, texture, position);
}

void Label::setText(string text, SDL_Renderer* renderer)
{
    this->text = text.c_str();
    update(renderer);
}