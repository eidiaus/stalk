#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"
#include "Enums.h"

#include <vector>
using std::vector;

class Player;
class NPC;
class Bullet;
class Vector2f;
class Wall;
class Label;
class Powerup;
class UIItem;
class Stage;
class GameObject;
class SpriteObject;
class Game 
{
private:
    // Declare window and renderer objects
    SDL_Window*	    gameWindow;
    SDL_Renderer*   gameRenderer;

    // Background texture
    SDL_Texture*    backgroundTexture;
    
    //Declare Player
    Player*         player;

    //Declare UI
    vector<UIItem*> playerHealth;
    Label* ScoreLabel;
    Label* StageLabel;
    Label* BossHealthLabel;

    SpriteObject* groundDecor;

    //Declare Stage
    vector<Stage*> stages;
    Stage* currentStage;
    int loadedStageID; //this will never be set by the program to -1, so its to bypass initialisation.

    //Declare NPC
    vector<NPC*>    npcs;

    // Walls
    vector<Wall*> walls;

    // Bullets
    vector<Bullet*> bullets;

    //Powerups
    vector<Powerup*> powerups;

    // Window control 
    bool            quit;
    
    // Keyboard
    const Uint8     *keyStates;

    // Game loop methods. 
    void processInputs();
    void update(float timeDelta);
    void draw();

public:
    // Constructor 
    Game();
    ~Game();

    // Methods
    void init();
    void runGameLoop();
    Player* getPlayer();
    Stage* getCurrentStage();
    void InitStage();
    void setCurrentStage(int stageid);
    void stageLogicHandler();
    void createStage(int enemycount, int enemytype);
    void createPowerup(Vector2f* position, int type);
    void createBullet(Vector2f* position, Vector2f* velocity, int parent);
    void createEnemy(Vector2f* position, int enemytype);
    void createWall(Vector2f* position, int width, int height);
    void loadStage();
    void collisionDetection();

    // Public attributes
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT= 600;
};

#endif