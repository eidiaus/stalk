#include "Wall.h"
#include "Vector2f.h"
#include "AABB.h"

/**
 * Wall
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */

Wall::Wall() : GameObject()
{
    setTag("Wall");
    targetRectangle.x = 0;
    targetRectangle.y = 0;

    scale = 1;
}

Wall::~Wall()
{

}

void Wall::init(SDL_Renderer* renderer, Vector2f* initPos, int width, int height)
{
    //init width, height and pos
    SPRITE_WIDTH = width;
    SPRITE_HEIGHT = height;
    targetRectangle.x = initPos->getX();
    targetRectangle.y = initPos->getY();

    //path string
    string texturePath("assets/images/brick-texture.png");

    // Call gameobject constructor
    GameObject::init(renderer, texturePath, initPos);

    // Setup the collider
    aabb = new AABB(this->getPosition(), targetRectangle.h, targetRectangle.w);
}
