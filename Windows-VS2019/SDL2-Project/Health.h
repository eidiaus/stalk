#ifndef HEALTH_H_
#define HEALTH_H_

#include "SDL2Common.h"
#include "Sprite.h"
#include "Vector2f.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Health
{
private:

    // Animation state
    int state;

    // Sprite information
    static const int SPRITE_HEIGHT = 9;
    static const int SPRITE_WIDTH = 9;

    Game* game;
    AABB* aabb;
    Vector2f* position;
public:
    Health();
    ~Health();


    void init(SDL_Renderer* renderer);
    

    void setGame(Game* game);

    //Overloads
    void update(float timeDeltaInSeconds);
    Vector2f* getPosition();
};



#endif