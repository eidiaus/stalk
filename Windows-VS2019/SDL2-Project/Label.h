#ifndef LABEL_H_
#define LABEL_H_

#include "GameObject.h"

// Forward declerations
// improve compile time. 

class Label : public GameObject
{
private:
    SDL_Surface* surface;
    const char* text;
    string path;
    TTF_Font* font;
    SDL_Color color;
public:
    Label();
    ~Label();

    // main initialiser
    void Initialise(SDL_Renderer* renderer, Vector2f* initPos, string text, TTF_Font* font, SDL_Color color);

    // initialising methods
    void init(SDL_Renderer* renderer, Vector2f* initPos, string text);
    void init(SDL_Renderer* renderer, Vector2f* initPos, string text, SDL_Color color);
    void init(SDL_Renderer* renderer, Vector2f* initPos, string text, TTF_Font* font);
    void init(SDL_Renderer* renderer, Vector2f* initPos, string text, TTF_Font* font, SDL_Color color);

    void update(SDL_Renderer* renderer);
    void setText(string text, SDL_Renderer* renderer);
};

#endif