#include "Powerup.h"
#include "AABB.h"
#include "Enums.h"
#pragma once

Powerup::Powerup()
{
    type = 0;
    setTag("Powerup");
    scale = 2;
}

Powerup::~Powerup()
{

}

void Powerup::init(SDL_Renderer* renderer, Vector2f* initPos, int type)
{
    targetRectangle.x = initPos->getX();
    targetRectangle.y = initPos->getY();

    this->type = type;

    //path string
    switch (this->type)
    {
    case POWERUP_HEALTH_POTION:
        texturePath = "assets/images/powerup_health_potion.png";
        break;
    case POWERUP_BULLET_ENHANCER:
        texturePath = "assets/images/powerup_bullet_enhancer.png";
        break;
    }

    // Call sprite constructor
    GameObject::init(renderer, texturePath, initPos);

    aabb = new AABB(this->getPosition(), targetRectangle.h, targetRectangle.w);
}

int Powerup::getType()
{
    return type;
}