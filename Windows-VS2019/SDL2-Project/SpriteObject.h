#ifndef SPRITEOBJECT_H_
#define SPRITEOBJECT_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Animation;

class SpriteObject : public Sprite
{
private:
    string path; //path to texture
    int state; //animation state for sprite handler
public:
    SpriteObject(); //Constructor
    ~SpriteObject(); //Destructor

    //Initialise method
    void init(SDL_Renderer* renderer, string path, Vector2f* position, int height, int width, float scale);

    enum AnimationStates { DEFAULT = 0 };

    int getCurrentAnimationState();

    //Overloads
    void update(float timeDeltaInSeconds);

};



#endif