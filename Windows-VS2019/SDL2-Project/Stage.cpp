#include "Stage.h"
#include <cstddef>
/**
 * Stage
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */
Stage::Stage()
{
	enemycount = NULL;
	enemytype = NULL;
	stageid = NULL;
}

Stage::~Stage()
{

}

void Stage::init(int stageid, int enemycount, int enemytype) //init stage with passed through values
{
	this->stageid = stageid;
	this->enemycount = enemycount;
	this->enemytype = enemytype;
}


int Stage::getEnemyCount()
{
	return enemycount;
}

int Stage::getEnemyType()
{
	return enemytype;
}

int Stage::getStageID()
{
	return stageid;
}