#ifndef POWERUP_H_
#define POWERUP_H_

#include "GameObject.h"

class Powerup : public GameObject
{
private:
	int type; //PowerupType Enums type identifier
	string texturePath;

public:
	Powerup(); //Constructor
	~Powerup(); //Destructor

	//Initialise method
	void init(SDL_Renderer* renderer, Vector2f* initPos, int type);
	int getType();
};

#endif