#include "Sprite.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "AABB.h"

#include <stdexcept>

/**
 * Sprite
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Sprite::Sprite()
{
    speed = 50.0f;

    //Velocity
    velocity = nullptr;

    // Animation array and size
    animations = nullptr;
    maxAnimations = 0;
    
}

/**
 * initSprite
 * 
 * Function to populate an sprite structure from given paramters. 
 * 
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @param maxAnimations total number of animations 
 * @param texturePath path to texture
 * @param position - initial position. 
 * @exception Throws an exception on file not found or out of memory. 
 */
void Sprite::init(SDL_Renderer *renderer, string texturePath, int maxAnimations, Vector2f* initPos)
{
    GameObject::init(renderer, texturePath, initPos);
        //setup max animations
    this->maxAnimations = maxAnimations;

    velocity = new Vector2f();
    velocity->zero();

    // create array of pointers to animations. 
    // child classes will have to init these!
    animations = new Animation * [maxAnimations];

    // Allocate memory for the animation structures
    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i] = new Animation();
    }

}


void Sprite::init(SDL_Renderer* renderer, SDL_Texture* texture, int maxAnimations, Vector2f* initPos)
{
    GameObject::init(renderer, texture, initPos);
    //setup max animations
    this->maxAnimations = maxAnimations;

    velocity = new Vector2f();
    velocity->zero();

    // create array of pointers to animations. 
    // child classes will have to init these!
    animations = new Animation * [maxAnimations];

    // Allocate memory for the animation structures
    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i] = new Animation();
    }
}


/**
 * ~Sprite
 * 
 * Destroys the Sprite and any associated 
 * objects 
 * 
 */
Sprite::~Sprite()
{
    // Deallocate memory
    // set pointers back to null (stale pointer).
    delete velocity;
    velocity = nullptr;

    // Clean up animations - free memory
    for (int i = 0; i < maxAnimations; i++)
    {
        // Clean up the animaton structure
        // allocated with new so use delete.
        delete animations[i];
        animations[i] = nullptr;
    }

    delete [] animations;
    animations = nullptr;
}

/**
 * draw
 * 
 * Method draw a Player object
 * 
 * @param renderer SDL_Renderer to draw to
 */
void Sprite::draw(SDL_Renderer *renderer)
{
    // Get current animation based on the state.
    Animation *current = this->animations[getCurrentAnimationState()];

    SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle);
}

/**
 * update
 * 
 * Method to update the Sprite 
 * 
 * @param timeDeltaInSeconds the time delta in seconds
 */
void Sprite::update(float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    Vector2f movement(velocity);
    movement.scale(timeDeltaInSeconds);

    // Update player position.
    position->add(&movement);

    // Move sprite to nearest pixel location.
    targetRectangle.x = round(position->getX());
    targetRectangle.y = round(position->getY());

    // Get current animation
    Animation *current = animations[getCurrentAnimationState()];

    // let animation update itself.
    current->update(timeDeltaInSeconds);

    // Move bounding box
    if(aabb != nullptr)
        aabb->setPosition(position);
}