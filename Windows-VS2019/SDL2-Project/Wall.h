#ifndef WALL_H_
#define WALL_H_

#include "GameObject.h"


class Wall : public GameObject
{
public:
    Wall();
    ~Wall();

    //Initialise method
    void init(SDL_Renderer* renderer, Vector2f* initPos, int width, int height);

};

#endif