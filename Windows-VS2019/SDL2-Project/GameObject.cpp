#include "GameObject.h"
#include "TextureUtils.h"
#include <stdexcept>

GameObject::GameObject()
{
    //Postion
    position = nullptr;

    // Set texture pointer
    texture = nullptr;

    // Bounding box - can only 
    // be setup in derived class
    aabb = nullptr;
}

GameObject::~GameObject()
{
    // Deallocate memory
// set pointers back to null (stale pointer).
    delete position;
    position = nullptr;

    delete aabb;
    aabb = nullptr;

    // Clean up
    SDL_DestroyTexture(texture);
    texture = nullptr;
}

AABB* GameObject::getAABB()
{
    return aabb;
}

Vector2f* GameObject::getPosition()
{
    return position;
}


void GameObject::setPosition(Vector2f* pos)
{
    position->setX(pos->getX());
    position->setY(pos->getY());
}

void GameObject::init(SDL_Renderer* renderer, string path, Vector2f* initPos) //init gameobject from texture path
{
    // Allocate position and velocity - set values
    position = new Vector2f(initPos);

    // Create texture from file, optimised for renderer
    texture = createTextureFromFile(path.c_str(), renderer);

    //if (texture == nullptr)
    //    throw std::runtime_error("Texture not found!"); disabled causes instability - needs to be looked into later.

    if (SPRITE_WIDTH == NULL) //check if we have a sprite_width defined or not
    {
        SDL_QueryTexture(texture, NULL, NULL, &targetRectangle.w, NULL); //populate targetRectangle's width with the width we queried from the texture
        if (targetRectangle.w == NULL) //if it still returns NULL set it to 0, it just won't display on screen in that case.
            targetRectangle.w = 0;
    }
    else
        targetRectangle.w = SPRITE_WIDTH; //if we do then set the targetRectangle's width to it.

    if (SPRITE_HEIGHT == NULL) //check if we have a sprite_height defined or not
    {
        SDL_QueryTexture(texture, NULL, NULL, NULL, &targetRectangle.h); //populate targetRectangle's height with the height we queried from the texture
        if (targetRectangle.h == NULL) //if it still returns NULL set it to 0, it just won't display on screen in that case.
            targetRectangle.h = 0;
    }
    else
        targetRectangle.h = SPRITE_HEIGHT; //if we do then set the targetRectangle's height to it.

    targetRectangle.h *= scale; //after we have retrieved all the information we need, let's multiply the height
    targetRectangle.w *= scale; //and width by the scalar.
}

void GameObject::init(SDL_Renderer* renderer, SDL_Texture* texture, Vector2f* initPos) //init gameobject from texture, i.e. for SDL_CreateTextureFromSurface() methods.
{
    // Allocate position and velocity - set values
    position = new Vector2f(initPos);

    this->texture = texture;

    if (SPRITE_WIDTH == NULL) //check if we have a sprite_width defined or not
    {
        SDL_QueryTexture(texture, NULL, NULL, &targetRectangle.w, NULL); //populate targetRectangle's width with the width we queried from the texture
        if (targetRectangle.w == NULL) //if it still returns NULL set it to 0, it just won't display on screen in that case.
            targetRectangle.w = 0;
    }
    else
        targetRectangle.w = SPRITE_WIDTH; //if we do then set the targetRectangle's width to it.

    if (SPRITE_HEIGHT == NULL) //check if we have a sprite_height defined or not
    {
        SDL_QueryTexture(texture, NULL, NULL, NULL, &targetRectangle.h); //populate targetRectangle's height with the height we queried from the texture
        if (targetRectangle.h == NULL) //if it still returns NULL set it to 0, it just won't display on screen in that case.
            targetRectangle.h = 0;
    }
    else
        targetRectangle.h = SPRITE_HEIGHT; //if we do then set the targetRectangle's height to it.

    targetRectangle.h *= scale; //after we have retrieved all the information we need, let's multiply the height
    targetRectangle.w *= scale; //and width by the scalar.
}

void GameObject::draw(SDL_Renderer* renderer)
{
    SDL_RenderCopy(renderer, texture, NULL, &targetRectangle);
}

bool GameObject::isVisible() //is the gameobject visible?
{
    return visible;
}

void GameObject::hide() //hide the game object
{
    this->visible = false;
}

void GameObject::show() //show the game object
{
    this->visible = true;
}

void GameObject::setTag(string tag)
{
    this->tag = tag;
}

string GameObject::getTag()
{
    return tag;
}