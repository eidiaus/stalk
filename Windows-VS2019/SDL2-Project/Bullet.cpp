#include "Bullet.h"
#include "AABB.h"
#include "Enums.h"
#include "Animation.h"

/**
 * Bullet
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */
Bullet::Bullet() : Sprite()
{
	setTag("Bullet");
	state = TRAVEL;
	speed = 200.0f;
	damage = 10;
	lifetime = 5.0f;
	SPRITE_WIDTH = 32;
	SPRITE_HEIGHT = 32;
	scale = 0.5;
	angleOffset = -90.0f;
	parent = NULL;
	orientation = NULL;
}

Bullet::~Bullet()
{
}

void Bullet::init(SDL_Renderer* renderer, Vector2f* initPos, Vector2f* direction, int parent) //init bullet
{
	//init bullet with pass through values
	this->parent = parent;
	this->orientation = calculateOrientation(direction);
	targetRectangle.x = initPos->getX();
	targetRectangle.y = initPos->getY();

	texturePath = "assets/images/orb_red.png";

	// Call sprite constructor
	Sprite::init(renderer, texturePath, 1, initPos);

	velocity->setX(direction->getX());
	velocity->setY(direction->getY());
	velocity->normalise();
	velocity->scale(speed);

	animations[TRAVEL]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0, 0);

	for (int i = 0; i < maxAnimations; i++)
	{
		animations[i]->setMaxFrameTime(0.2f);
	}

	aabb = new AABB(this->getPosition(), targetRectangle.h, targetRectangle.w);

}

float Bullet::calculateOrientation(Vector2f* direction)
{
	float angle = atan2f(direction->getY(), direction->getX()); //get angle (0,2pi)
	angle *= (180.0f / 3.142f); //convert to degrees
	return angle + angleOffset;
}

void Bullet::update(float dt)
{
	lifetime -= dt;
	Sprite::update(dt);
}
int Bullet::getCurrentAnimationState()
{
	return state;
}
int Bullet::getDamage()
{
	return damage;
}

void Bullet::draw(SDL_Renderer* renderer) //custom draw function allowing for angle/orientation to be accounted for and drawn
{
	Animation* current = this->animations[getCurrentAnimationState()];
	SDL_RenderCopyEx(renderer, texture, current->getCurrentFrame(), &targetRectangle, orientation, nullptr, SDL_FLIP_NONE);
}

bool Bullet::hasExpired() //has bullet been up for too long? return true
{
	if (lifetime > 0.0f)
		return false;
	else
		return true;

}

int Bullet::getParent() //who fired the bullet?
{
	return parent;
}

void Bullet::setParent(int parent) //set entity to bullet owner - parent flags from Parent Enum
{
	this->parent = parent;
}
