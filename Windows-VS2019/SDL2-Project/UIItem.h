#ifndef UIItem_H_
#define UIITEM_H_

#include "GameObject.h"

class UIItem : public GameObject
{
private:
	string texturePath; //path to texture
	int type; //UIType Enums type identifier
public:
	UIItem(); //Constructor
	~UIItem(); //Destructor

	//Initialise method
	void init(SDL_Renderer* renderer, Vector2f* position);
	void update(SDL_Renderer* renderer);
	void setType(SDL_Renderer* renderer, int type);
	void setScale(SDL_Renderer* renderer, float scale);
	float getScale();
	int getType();
};
#endif