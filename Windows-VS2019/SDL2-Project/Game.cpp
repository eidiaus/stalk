﻿#include "Game.h"
#include "TextureUtils.h"
#include "SDL2Common.h"
#include "Player.h"
#include "NPC.h"
#include "Bullet.h"
#include "Vector2f.h"
#include "AABB.h"
#include "Wall.h"
#include "Label.h"
#include "Powerup.h"
#include "UIItem.h"
#include "Stage.h"
#include "SpriteObject.h"
#include <iostream>
#include <chrono>
#include <thread>
using namespace std;
//for printf
#include <cstdio>

// for exceptions
#include <stdexcept>


/**
 * Game
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */
Game::Game() 
{
    gameWindow = nullptr;
    gameRenderer = nullptr;
    currentStage = nullptr;
    loadedStageID = -1;
    BossHealthLabel = nullptr;
    ScoreLabel = nullptr;
    StageLabel = nullptr;
    groundDecor = nullptr;
    backgroundTexture = nullptr;
    player = nullptr;

    for (vector<NPC*>::iterator it = npcs.begin(); it != npcs.end(); ++it)
    {
        (*it) = nullptr;
    }

    for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end(); ++it)
    {
        (*it) = nullptr;
    }

    for (vector<Stage*>::iterator it = stages.begin(); it != stages.end(); ++it)
    {
        (*it) = nullptr;
    }

    for (vector<UIItem*>::iterator it = playerHealth.begin(); it != playerHealth.end(); ++it)
    {
        (*it) = nullptr;
    }

    for (vector<Powerup*>::iterator it = powerups.begin(); it != powerups.end(); ++it)
    {
        (*it) = nullptr;
    }

    for (vector<Wall*>::iterator it = walls.begin(); it != walls.end(); ++it)
    {
        (*it) = nullptr;
    }

    keyStates = nullptr;

    quit = false;
}

void Game::init()
{
    gameWindow = SDL_CreateWindow("Stalk",   // Window title
        SDL_WINDOWPOS_UNDEFINED, // X position
        SDL_WINDOWPOS_UNDEFINED, // Y position
        WINDOW_WIDTH,            // width
        WINDOW_HEIGHT,           // height               
        SDL_WINDOW_SHOWN);       // Window flags  

    if (gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if (gameRenderer == nullptr)
        {
            throw std::runtime_error("Error - SDL could not create renderer\n");
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        throw std::runtime_error("Error - SDL could not create Window\n");
    }

    // Track Keystates array
    keyStates = SDL_GetKeyboardState(NULL);

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/background.png", gameRenderer);

    if (backgroundTexture == nullptr)
        throw std::runtime_error("Background image not found\n");

    //Score Label
    ScoreLabel = new Label();
    ScoreLabel->init(gameRenderer, new Vector2f(0.0f, -11.0f), "Player Score: 0");

    //Boss Health Label
    BossHealthLabel = new Label();
    BossHealthLabel->init(gameRenderer, new Vector2f(float((WINDOW_WIDTH / 2)-100), 0.0f), "Boss HP: 0%");

    for (int i = 0; i < 3; i++) //create 3 hearts
    {
        UIItem* Heart = new UIItem();
        Heart->init(gameRenderer, new Vector2f(float(WINDOW_WIDTH - 150 + (40 * i)), 0.0f));
        Heart->setScale(gameRenderer, 2);
        Heart->setType(gameRenderer, UI_FULL_HEART);
        playerHealth.push_back(Heart);
    }

    //Stage Text Label
    StageLabel = new Label();
    StageLabel->init(gameRenderer, new Vector2f((WINDOW_WIDTH / 2) - 40, (WINDOW_HEIGHT / 2) - 50), "Stage X");

    //setup player
    player = new Player();
    player->init(gameRenderer);
    player->setGame(this);

    //setup stages
    createStage(10, ENEMY_MELEE);
    createStage(10, ENEMY_MELEE + ENEMY_CASTER);
    createStage(15, ENEMY_MELEE + ENEMY_CASTER);
    createStage(10, ENEMY_MELEE + ENEMY_CASTER + ENEMY_BOSS);
    currentStage = stages[0];

    //create walls
    createWall(new Vector2f(0,0), 50, 1);
    createWall(new Vector2f(0, 0), 1, 40);
    createWall(new Vector2f(0, WINDOW_HEIGHT-16), 50, 1);
    createWall(new Vector2f(WINDOW_WIDTH - 16, 0), 1, 50);

    //create spriteobjects
    groundDecor = new SpriteObject();
    groundDecor->init(gameRenderer, "assets/images/floor_effect_spritesheet.png", new Vector2f((WINDOW_WIDTH / 2) - 30, (WINDOW_HEIGHT / 2) - 10), 512, 512, 0.125);

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
}

Game::~Game()
{
    //Clean up!
    delete player;
    player = nullptr;  

    delete groundDecor;
    groundDecor = nullptr;

    delete ScoreLabel;
    ScoreLabel = nullptr;

    delete StageLabel;
    StageLabel = nullptr;

    delete BossHealthLabel;
    BossHealthLabel = nullptr;

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end();)
    {   
        delete *it;
        *it = nullptr;
        it = bullets.erase(it);
    } 

    for (vector<NPC*>::iterator it = npcs.begin(); it != npcs.end();)
    {
        delete* it;
        *it = nullptr;
        it = npcs.erase(it);
    }

    for (vector<Stage*>::iterator it = stages.begin(); it != stages.end();)
    {
        delete* it;
        *it = nullptr;
        it = stages.erase(it);
    }

    for (vector<UIItem*>::iterator it = playerHealth.begin(); it != playerHealth.end();)
    {
        delete* it;
        *it = nullptr;
        it = playerHealth.erase(it);
    }

    for (vector<Powerup*>::iterator it = powerups.begin(); it != powerups.end();)
    {
        delete* it;
        *it = nullptr;
        it = powerups.erase(it);
    }

    for (vector<Wall*>::iterator it = walls.begin(); it != walls.end();)
    {
        delete* it;
        *it = nullptr;
        it = walls.erase(it);
    }

    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   
}


void Game::draw()
{
     // 1. Clear the screen
    SDL_RenderClear(gameRenderer);

    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

    if (groundDecor->isVisible())
        groundDecor->draw(gameRenderer);

    for (int i = 0; i < powerups.size(); i++)
        if (powerups[i]->isVisible())
            powerups[i]->draw(gameRenderer);

    if (player->isVisible())
        player->draw(gameRenderer);

    for (int i = 0; i < npcs.size(); i++)
        if (npcs[i]->isVisible())
            npcs[i]->draw(gameRenderer);
    
    /*for (int i = 0; i < walls.size(); i++)
       walls[i]->draw(gameRenderer);*/

    for(int i = 0; i < bullets.size(); i++)
        if (bullets[i]->isVisible())
            bullets[i]->draw(gameRenderer);                      //Only draw objects to the screen if isVisible()
    
   /* for (int i = 0; i < uiitems.size(); i++)
        uiitems[i]->draw(gameRenderer);*/

    // 2.5 Draw HUD

    for (int i = 0; i < playerHealth.size(); i++)
        if (playerHealth[i]->isVisible())
            playerHealth[i]->draw(gameRenderer);

    if (ScoreLabel->isVisible())
        ScoreLabel->draw(gameRenderer);

    if (StageLabel->isVisible())
        StageLabel->draw(gameRenderer);

    if (BossHealthLabel->isVisible())
        BossHealthLabel->draw(gameRenderer);


    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    
}

bool stageIntroActive = true;
int stageIntroTimer = 0;
int spawnedEnemyCount = 0; //local variables declaration
int enemySpawnTimer = 0;
int enemyType = 0;

void Game::loadStage() //load stage method
{
    if (currentStage->getStageID() != loadedStageID)
    {
        loadedStageID = getCurrentStage()->getStageID();
        InitStage();
        stageIntroActive = true;
    }


    if (stageIntroActive == true)
    {
        if (stageIntroTimer >= 1000)
        {
            stageIntroActive = false;
            SDL_DestroyTexture(backgroundTexture);
            backgroundTexture = createTextureFromFile("assets/images/background.png", gameRenderer);
            StageLabel->hide();
            player->show();
            player->init(gameRenderer);
            for (int i = 0; i < powerups.size(); i++)
                powerups[i]->show();
            groundDecor->show();
        }
        else
        {
            stageIntroTimer++;
            player->hide();
            SDL_DestroyTexture(backgroundTexture);
            backgroundTexture = SDL_CreateTexture(gameRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, WINDOW_WIDTH, WINDOW_HEIGHT);
            StageLabel->setText(("Stage " + std::to_string(currentStage->getStageID() + 1)), gameRenderer);
        }
    }

}


void Game::stageLogicHandler() //handle stage logic
{
    if (stageIntroActive == false)
    {
        if (player->getKills() < currentStage->getEnemyCount())
        {
            if (spawnedEnemyCount < currentStage->getEnemyCount())
            {
                if (enemySpawnTimer >= 300)
                {
                    switch (currentStage->getEnemyType())
                    {
                    case ENEMY_MELEE + ENEMY_CASTER: //if melee+caster wave
                        if (rand() % 2 == 0) //50% chance of it being melee or caster
                            enemyType = ENEMY_MELEE;
                        else
                            enemyType = ENEMY_CASTER;
                        break;
                    case ENEMY_CASTER + ENEMY_BOSS: //if caster+boss wave
                        if (spawnedEnemyCount < currentStage->getEnemyCount() - 1)
                            enemyType = ENEMY_CASTER; //spawn casters until there is only room for 1 more enemy. then bring out the boss
                        else
                            enemyType = ENEMY_BOSS;
                        break;
                    case ENEMY_MELEE + ENEMY_BOSS:
                        if (spawnedEnemyCount < currentStage->getEnemyCount() - 1) //if melee+boss wave
                            enemyType = ENEMY_MELEE; //spawn meleers until there is only room for 1 more enemy. then bring out the boss
                        else
                            enemyType = ENEMY_BOSS;
                        break;
                    case ENEMY_MELEE + ENEMY_CASTER + ENEMY_BOSS: //if melee+caster+boss wave
                        if (spawnedEnemyCount < currentStage->getEnemyCount() - 1)//spawn others until there is only room for 1 more enemy. then bring out the boss
                        {
                            if (rand() % 2 == 0) //50% chance of it being melee or caster
                                enemyType = ENEMY_MELEE;
                            else
                                enemyType = ENEMY_CASTER;
                        }
                        else
                            enemyType = ENEMY_BOSS;
                        break;
                    default: //if not a combination wave then set enemytype to stage->getEnemyType()
                        enemyType = currentStage->getEnemyType();
                        break;
                    }



                    int spawnPos = rand() % 3; //3 random spawn positions - left, right and bottom

                    switch (spawnPos) //spawn the enemy in with the generated position and calculated enemytype.
                    {
                    case 0:
                        createEnemy(new Vector2f(0.0f, float(rand() % WINDOW_HEIGHT)), enemyType);
                        break;
                    case 1:
                        createEnemy(new Vector2f(float(WINDOW_WIDTH), float(rand() % WINDOW_HEIGHT)), enemyType);
                        break;
                    case 2:
                        createEnemy(new Vector2f(float(rand() % WINDOW_WIDTH), float(WINDOW_HEIGHT)), enemyType);
                        break;
                    }
                    if (enemyType == ENEMY_BOSS)
                    {
                        BossHealthLabel->show(); //show the boss's health if hes about to spawn.
                    }
                    spawnedEnemyCount += 1; //increment spawnedEnemyCount
                    enemySpawnTimer = 0; //reset enemySpawnTimer
                }
                else
                {
                    enemySpawnTimer++; //increment enemySpawnTimer
                }
            }
        }
        else
        {
            if (currentStage->getStageID() < stages.size() - 1) //if there are more stages to complete then set the stage to the next stage in the vector.
                setCurrentStage(currentStage->getStageID() + 1);
            else
            {
                StageLabel->setText("You win!", gameRenderer); //else game has been cleared and present "you win!" screen
                for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();) //free up some memory resources, etc... hide and show necessary components for the scene..
                {
                    delete* it;
                    *it = nullptr;
                    it = bullets.erase(it);
                }

                for (vector<NPC*>::iterator it = npcs.begin(); it != npcs.end();)
                {
                    delete* it;
                    *it = nullptr;
                    it = npcs.erase(it);
                }

                for (vector<Powerup*>::iterator it = powerups.begin(); it != powerups.end();)
                {
                    delete* it;
                    *it = nullptr;
                    it = powerups.erase(it);
                }
                groundDecor->hide();
                player->hide();
                SDL_DestroyTexture(backgroundTexture);
                backgroundTexture = SDL_CreateTexture(gameRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, WINDOW_WIDTH, WINDOW_HEIGHT);
                StageLabel->show();

            }
        }
    }
}


void Game::InitStage() //init stage values
{
    for (int i = 0; i < powerups.size(); i++)
    {
        powerups[i]->hide();
    }
    groundDecor->hide();
    BossHealthLabel->hide();
    player->setKills(0);
    stageIntroTimer = 0;

    for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
    {
        delete* it;
        *it = nullptr;
        it = bullets.erase(it);
    }

    for (vector<NPC*>::iterator it = npcs.begin(); it != npcs.end();)
    {
        delete* it;
        *it = nullptr;
        it = npcs.erase(it);
    }

    player->hide();
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = SDL_CreateTexture(gameRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, WINDOW_WIDTH, WINDOW_HEIGHT);
    StageLabel->show();
    spawnedEnemyCount = 0;
}

int immunitytimer = 600;
bool gameover = false;
void Game::update(float timeDelta)
{
    if(!gameover) //if player isnt dead
    {

        player->update(timeDelta);

        groundDecor->update(timeDelta);

        for (vector<NPC*>::iterator it = npcs.begin(); it != npcs.end(); ++it)
        {
            (*it)->update(timeDelta);
        }
        if (!player->getAABB()->isActive()) //if the players collider is not active then
        {
            immunitytimer--; //decrement immunitytimer
            if (immunitytimer == 0)//once the desired time has been reached set players collider to active and reset immunitytimer's value
            {
                player->getAABB()->setActive(true);
                immunitytimer = 600;
            }
        }
        for (int i = 0; i < playerHealth.size(); i++) //resetting playerHealth to 0
        {
            playerHealth[i]->setType(gameRenderer, UI_EMPTY_HEART);
        }

        //3 hearts, 6 lives -> 2 lives per heart.
        //heart.type += 1 until heart.type == 2. 
        //if heart.type == 2 then move to next heart

        int lives = player->getLives();
        if (lives == 0)
        {
            StageLabel->setText("Game Over", gameRenderer);
            InitStage();
            gameover = true;

        }
        //keep looping until lives = 0

        while (lives > 0)
        {
            if (playerHealth[0]->getType() < UI_FULL_HEART)
            {
                playerHealth[0]->setType(gameRenderer, playerHealth[0]->getType() + 1);
            }
            else if (playerHealth[1]->getType() < UI_FULL_HEART)
            {
                playerHealth[1]->setType(gameRenderer, playerHealth[1]->getType() + 1);
            }
            else if (playerHealth[2]->getType() < UI_FULL_HEART)
            {
                playerHealth[2]->setType(gameRenderer, playerHealth[2]->getType() + 1);
            }
            lives -= 1;
        }

        for (int i = 0; i < npcs.size(); i++)
        {
            if (npcs[i]->getType() == ENEMY_BOSS)
            {
                if (npcs[i]->isDead())
                {
                    BossHealthLabel->hide(); //hide boss health if boss is dead
                    break;
                }
                else
                {
                    std::stringstream bosshealthstrstream;
                    float bossCurrentHealthPercentage = ((float)npcs[i]->getHealth() / (float)npcs[i]->getMaxHealth()) * 100; //calculate boss health as percentage
                    bosshealthstrstream << "Boss HP: " << bossCurrentHealthPercentage << "%";
                    string bosshealthstr = bosshealthstrstream.str();

                    BossHealthLabel->setText(bosshealthstr, gameRenderer); //update text label to boss health percentage
                    //original method to get boss health, returns float with extra decimal places - unrequired.
                    //BossHealthLabel->setText(("Boss HP: " + std::to_string(((float)npcs[i]->getHealth() / (float)npcs[i]->getMaxHealth()) * 100) + "%").c_str(), gameRenderer);
                    break;
                }
            }
        }

        //fetch score and set score
        ScoreLabel->setText(("Player Score: " + std::to_string(player->getScore())).c_str(), gameRenderer);

        //loadstage - onlys runs if (currentStage->getStageID() != loadedStageID)
        loadStage();

        //handle all stage logic here
        stageLogicHandler();

        //check if bullet has expired, if so delete it
        for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
        {
            if ((*it)->hasExpired())
            {
                delete* it;
                *it = nullptr;
                it = bullets.erase(it);
            }
            else
            {
                ++it;
            }
        }

        for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end(); ++it)
        {
            (*it)->update(timeDelta);
        }

        collisionDetection();

    }
}

void Game::processInputs()
{
    SDL_Event event;

    // Handle input 
    if( SDL_PollEvent( &event ))  // test for events
    { 
        switch(event.type) 
        { 
            case SDL_QUIT:
                quit = true;
            break;

            // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                }
            break;

            // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                }
            break;
            
            default:
                // not an error, there's lots we don't handle. 
                break;    
        }
    }

    // Process Inputs - for player 
    player->processInput(keyStates);
}

Player* Game::getPlayer() 
{
    return player;
}

Stage* Game::getCurrentStage()
{
    return currentStage;
}

void Game::runGameLoop()
{
    // Timing variables
    unsigned int currentTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;
    unsigned int prevTimeIndex;

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist 
        //- https://gafferongames.com/post/fix_your_timestep/
        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Process inputs
        processInputs();

        // Update Game Objects
        update(timeDeltaInSeconds);

        //Draw stuff here.
        draw();

        SDL_Delay(1);
    }
}

//create object functions

void Game::createStage(int enemycount, int enemytype)
{
    Stage* stage = new Stage();
    stage->init(static_cast<int>(stages.size()), enemycount, enemytype);
    stages.push_back(stage);
}


void Game::createPowerup(Vector2f* position, int type)
{
    Powerup* powerup = new Powerup();
    powerup->init(gameRenderer, position, type);

    powerups.push_back(powerup);
}

void Game::createBullet(Vector2f* position, Vector2f* velocity, int parent)
{

    Bullet* bullet = new Bullet();
    bullet->init(gameRenderer, position, velocity, parent);

    bullets.push_back(bullet);
}

void Game::createEnemy(Vector2f* initPos, int enemytype)
{
    NPC * enemy = new NPC();
    enemy->init(gameRenderer, initPos, enemytype);
    enemy->setGame(this);

    npcs.push_back(enemy);
}

void Game::createWall(Vector2f* initPos, int width, int height)
{
    Wall* wall = new Wall();
    wall->init(gameRenderer, initPos, width*16, height*16);

    walls.push_back(wall);

}

void Game::setCurrentStage(int stageid)
{
    currentStage = stages[stageid];
}



void Game::collisionDetection()
{
    for (int i = 0; i < npcs.size(); i++) //if player hits npc with bullet then execute
    {
        for (int j = 0; j < bullets.size(); j++)
        {
            if (bullets[j]->getAABB()->intersects(npcs[i]->getAABB()) && bullets[j]->getParent() == PARENT_PLAYER)
            {
                if (!npcs[i]->isDead())
                {
                    player->addScore(npcs[i]->getPoints());
                    npcs[i]->takeDamage(bullets[j]->getDamage());
                    delete(bullets[j]);
                    bullets[j] = nullptr;
                    bullets.erase(bullets.begin() + j);
                    break;
                }
            }
        }
    }

    if (player->getAABB()->isActive()) //if player is not immune then execute
    {
        for (int i = 0; i < bullets.size(); i++) //if npc hits player with bullet then execute - additional check for boss as different PARENT flag
        {
            if (bullets[i]->getAABB()->intersects(player->getAABB()) && (bullets[i]->getParent() == PARENT_NPC || bullets[i]->getParent() == PARENT_BOSS))
            {
                player->getAABB()->setActive(false);
                player->removeLife();
                delete(bullets[i]);
                bullets[i] = nullptr;
                bullets.erase(bullets.begin() + i);
            }
        }
    }


    if (player->getAABB()->isActive()) //if player is not immune then execute
    {
        for (int i = 0; i < npcs.size(); i++) //if npc hits player (only non casters can do this) then execute
        {
            if (npcs[i]->getAABB()->intersects(player->getAABB()) && npcs[i]->getType() != ENEMY_CASTER && !npcs[i]->isDead())
            {
                player->getAABB()->setActive(false);
                player->removeLife();
                break;
            }
        }
    }

    for (int i = 0; i < powerups.size(); i++) //if player picks up a powerup then execute
    {
        if (powerups[i]->getAABB()->intersects(player->getAABB()))
        {
            switch (powerups[i]->getType())
            {
            case POWERUP_HEALTH_POTION:
                if (player->getLives() == player->getMaxLives())
                    player->addScore(100);
                else
                    player->addLife();
                break;
            case POWERUP_BULLET_ENHANCER:
                if (player->getCooldown() == 0.10f)
                    player->addScore(100);
                else
                    player->setCooldown(player->getCooldown() - 0.05f);
                break;
            }
            delete(powerups[i]);
            powerups[i] = nullptr;
            powerups.erase(powerups.begin() + i);
        }
    }

    for (int i = 0; i < walls.size(); i++) //if a bullet hits the wall then execute
    {
        for (int j = 0; j < bullets.size(); j++)
        {
            if (bullets[j]->getAABB()->intersects(walls[i]->getAABB()) && bullets[j]->getParent() != PARENT_BOSS) //if the bullet doesnt belong to a boss then execute
            {
                delete(bullets[j]);
                bullets[j] = nullptr;
                bullets.erase(bullets.begin() + j);
                break;
            }
        }
    }

}