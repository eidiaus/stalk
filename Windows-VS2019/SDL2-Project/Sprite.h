#ifndef SPRITE_H_
#define SPRITE_H_

#include "GameObject.h"

// Forward declarations
// improve compile time.
class Animation;

class Sprite : public GameObject
{
protected:
    
    float speed;
    Vector2f* velocity;
    Vector2f idleVelocity;
    //// Animations
    int maxAnimations;
    Animation** animations;


public:
    Sprite();
    virtual ~Sprite();

    virtual void init(SDL_Renderer *renderer, string texturePath, int maxAnimations, Vector2f* position);

    virtual void init(SDL_Renderer *renderer, SDL_Texture* tex, int maxAnimations, Vector2f* position);

    virtual void draw(SDL_Renderer *renderer);
    
    virtual void update(float timeDeltaInSeconds);

    virtual int getCurrentAnimationState() = 0;


};

#endif