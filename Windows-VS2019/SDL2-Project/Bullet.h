#ifndef BULLET_H_
#define BULLET_H_

#include "Sprite.h"

class Bullet : public Sprite
{
private:
    
    // Animation state
    int state;

    string texturePath;
    int damage;
    float lifetime;
    
    // angle in radians
    float orientation;

    // don't know the direction of the sprite
    // in the image I have?
    float angleOffset;
    float calculateOrientation(Vector2f* direction);

    //who shot the bullet?
    int parent;

public:
    Bullet();
    ~Bullet();

    // Player Animation states
    enum BulletState{TRAVEL = 0};
    
    void init(SDL_Renderer *renderer, Vector2f* position, Vector2f* direction, int parent);
    void update(float dt);
    void draw(SDL_Renderer *renderer);

    int getCurrentAnimationState();

    int getDamage();
    bool hasExpired();

    int getParent();
    void setParent(int parent);

};

#endif