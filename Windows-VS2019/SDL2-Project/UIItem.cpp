#include "UIItem.h"
#include "TextureUtils.h"
#include "Enums.h"
/**
 * UIItem
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */
UIItem::UIItem()
{
	setTag("UIItem");
	type = 0;
}

UIItem::~UIItem()
{

}

void UIItem::init(SDL_Renderer* renderer, Vector2f* position)
{
	//init pos
	targetRectangle.x = position->getX();
	targetRectangle.y = position->getY();

	switch (this->type)
	{
	case UI_FULL_HEART:
		texturePath = "assets/images/full_heart.png";
		break;
	case UI_HALF_HEART:
		texturePath = "assets/images/half_heart.png";
		break;
	case UI_EMPTY_HEART:
		texturePath = "assets/images/empty_heart.png";
		break;
	}

	//unknown width and height passed - we will work this out in the gameobject class:
	GameObject::init(renderer, texturePath, position);
}

void UIItem::update(SDL_Renderer* renderer)
{
	//recreate texture
	SDL_DestroyTexture(texture);
	texture = createTextureFromFile(texturePath.c_str(), renderer);
	
	//reinit gameobject
	GameObject::init(renderer, texture, position);
}


void UIItem::setType(SDL_Renderer* renderer, int type) //update texturepath and then pass to update method to handle
{
	this->type = type;
	switch (this->type)
	{
	case UI_FULL_HEART:
		texturePath = "assets/images/full_heart.png";
		break;
	case UI_HALF_HEART:
		texturePath = "assets/images/half_heart.png";
		break;
	case UI_EMPTY_HEART:
		texturePath = "assets/images/empty_heart.png";
		break;
	}

	update(renderer);
}

void UIItem::setScale(SDL_Renderer* renderer, float scale) //change scale of gameobject then pass to update method to handle - gameobject must be informed of 
{                                                          //scale change in order to multiply dimensions
	this->scale = scale;
	update(renderer);
}

float UIItem::getScale()
{
	return scale;
}

int UIItem::getType()
{
	return type;
}