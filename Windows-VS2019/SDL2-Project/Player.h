#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"
#include "Vector2f.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Player : public Sprite
{
private:

    int tile = 16;
    // Animation state
    int state;
    int previousState;

    // Score
    int points;
    int kills;
    int lives;

    // Bullet spawn
    float cooldownTimer;
    float COOLDOWN_MAX;
    int LIVES_MAX;

    // Need Game
    Game* game;

    // Need bulletPosition in order to work out idle velocity.
    //In hindsight I see this attribute is pointless as idleVelocity exists in the Sprite class.
    Vector2f bulletPosition;

public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT=0, RIGHT, UP, DOWN, IDLE};
    
    void init(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);

    void setGame(Game* game);
    void fire();

    void addScore(int points);
    int getScore();

    void addKill();
    void setKills(int kills);
    int getKills();

    float getCooldown();
    void setCooldown(float cooldown);

    void setLives(int lives);
    void addLife();
    void removeLife();

    int getLives();
    int getMaxLives();

    //Overloads
    void update(float timeDeltaInSeconds);

    int getCurrentAnimationState();
};



#endif