#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "Game.h"
#include "AABB.h"


// Only types with a fixed bit representaition can be
// defined in the header file

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Player::Player() : Sprite()
{
    game = nullptr;
    setTag("Player");
    COOLDOWN_MAX = 0.5f;
    SPRITE_HEIGHT = 64;
    SPRITE_WIDTH = 64;
    state = IDLE;
    previousState = LEFT;
    speed = 50.0f;
    scale = 0.7f;
    // Initialise weapon cooldown. 
    cooldownTimer = 0;

    // Init points
    points = 0;
    kills = 0;
    LIVES_MAX = 6;
    lives = LIVES_MAX;
}

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Player::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/necromancer_64.png");

    //postion
    Vector2f position(380.0f,280.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 5, &position);

    // Setup the animation structure
    animations[LEFT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1, 0);
    animations[RIGHT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0, 0);
    animations[UP]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3, 0);
    animations[DOWN]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2, 0);
    animations[IDLE]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), targetRectangle.h, targetRectangle.w);

}


/**
 * ~Player
 * 
 * Destroys the player and any associated 
 * objects 
 * 
 */
Player::~Player()
{

}

/**
 * processInput
 * 
 * Method to process inputs for the player 
 * Note: Need to think about other forms of input!
 * 
 * @param keyStates The keystates array. 
 */
void Player::processInput(const Uint8 *keyStates)
{
    if (this->isVisible())
    {
        // Process Player Input

        //Input - keys/joysticks?
        float verticalInput = 0.0f;
        float horizontalInput = 0.0f;
        //float speed = 1000.0f;

        // If no keys are down player should not animate!
        state = IDLE;

        // This could be more complex, e.g. increasing the vertical
        // input while the key is held down.
        if (keyStates[SDL_SCANCODE_UP] || keyStates[SDL_SCANCODE_W])
        {
            if (this->getPosition()->getY() >= 2 * tile)
            {
                animations[IDLE]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3, 0);
                verticalInput = -1.0f;
                state = previousState = UP;
            }
        }

        if (keyStates[SDL_SCANCODE_DOWN] || keyStates[SDL_SCANCODE_S])
        {
            if (this->getPosition()->getY() <= 35 * tile)
            {
                animations[IDLE]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2, 0);
                verticalInput = 1.0f;
                state = previousState = DOWN;
            }
        }

        if (keyStates[SDL_SCANCODE_RIGHT] || keyStates[SDL_SCANCODE_D])
        {
            if (this->getPosition()->getX() <= 49 * tile)
            {
                horizontalInput = 1.0f;
                state = previousState = RIGHT;
                animations[IDLE]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0, 0);
            }

        }

        if (keyStates[SDL_SCANCODE_LEFT] || keyStates[SDL_SCANCODE_A])
        {
            if (this->getPosition()->getX() >= 0 * tile)
            {
                horizontalInput = -1.0f;
                state = previousState = LEFT;
                animations[IDLE]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1, 0);
            }
        }

        // Calculate player velocity.
        velocity->setX(horizontalInput);
        velocity->setY(verticalInput);
        velocity->normalise();
        velocity->scale(speed);

        if (keyStates[SDL_SCANCODE_SPACE])
        {
            fire();
        }
    }
}

int Player::getCurrentAnimationState()
{
    return state;
}

void Player::setGame(Game* game)
{
    this->game = game;
}

void Player::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);

    cooldownTimer += timeDeltaInSeconds;

}

void Player::fire()
{   
     //Need a cooldown timer, otherwise we shoot 
     //a million bullets a second and our npc
     //dies instantly
    if(cooldownTimer > COOLDOWN_MAX)
    {
        bulletPosition = position;
        if ((velocity->length() > 0 && state != LEFT) || previousState != LEFT)
            bulletPosition.setX(bulletPosition.getX() + 23);

        if (velocity->length() > 0.0f)
            game->createBullet(&bulletPosition, velocity, PARENT_PLAYER);
        else
        {
            //bulletPosition
            switch (previousState)
            {
            case LEFT:
                game->createBullet(&bulletPosition, new Vector2f(-1, 0), PARENT_PLAYER);
                break;
            case RIGHT:
                game->createBullet(&bulletPosition, new Vector2f(1.0, 0), PARENT_PLAYER);
                break;
            case UP:
                game->createBullet(&bulletPosition, new Vector2f(0, -1), PARENT_PLAYER);
                break;
            case DOWN:
                game->createBullet(&bulletPosition, new Vector2f(0, 1), PARENT_PLAYER);
                break;
            default:
                game->createBullet(&bulletPosition, new Vector2f(1, 0), PARENT_PLAYER);
                break;
            }
        }
        cooldownTimer = 0;
    }
}

void Player::addScore(int points)
{
    this->points += points;
}

int Player::getScore()
{
    return points;
}

void Player::addKill()
{
    this->kills += 1;
}

void Player::setKills(int kills)
{
    this->kills = kills;
}


int Player::getKills()
{
    return kills;
}

float Player::getCooldown()
{
    return COOLDOWN_MAX;
}

void Player::setCooldown(float cooldown)
{
    if (cooldown <= 0.10f)
        cooldown = 0.10f;
    COOLDOWN_MAX = cooldown;
}

int Player::getLives()
{
    return lives;
}

void Player::setLives(int lives)
{
    if (lives > LIVES_MAX)
        this->lives = LIVES_MAX;
    else
        this->lives = lives;
}

void Player::addLife()
{
    if (this->lives < LIVES_MAX)
        lives += 1;
}

void Player::removeLife()
{
    if (lives > 0)
        lives -= 1;
}

int Player::getMaxLives()
{
    return LIVES_MAX;
}