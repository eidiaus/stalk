#include "SpriteObject.h"
#include "Animation.h"
#include "TextureUtils.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

// Only types with a fixed bit representaition can be
// defined in the header file

/**
 * SpriteObject
 *
 * Constructor, setup all the simple stuff. Set pointers to null etc.
 *
 */
SpriteObject::SpriteObject() : Sprite()
{
    state = DEFAULT;
    setTag("SpriteObject");
    SPRITE_HEIGHT = 64;
    SPRITE_WIDTH = 64;
    speed = 50.0f;
    scale = 1;
}

SpriteObject::~SpriteObject()
{

}

/**
 * initSpriteObject
 *
 * Function to populate an animation structure from given paramters.
 *
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory.
 */
void SpriteObject::init(SDL_Renderer* renderer, string path, Vector2f* position, int height, int width, float scale)
{

    //init position, size, scale and path from passed through values
    this->position = position;
    SPRITE_HEIGHT = height;
    SPRITE_WIDTH = width;
    this->scale = scale;
    this->path = path;

    // Call sprite constructor
    Sprite::init(renderer, path, 1, this->position);

    // Setup the animation structure
    animations[DEFAULT]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.08f);
    }

    aabb = new AABB(this->getPosition(), targetRectangle.h, targetRectangle.w);

}


int SpriteObject::getCurrentAnimationState()
{
    return state;
}


void SpriteObject::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);
}
